package com.example.magno_mac.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.magno_mac.ecualizacionimagenmc.R;
import com.example.magno_mac.models.Ecualizador;
import com.example.magno_mac.models.FiltroMediana;
import com.example.magno_mac.models.Histograma;

public class MainActivity extends AppCompatActivity {

    private Histograma histograma;
    private ImageView imgvResultado;
    private Bitmap imagenOriginal;
    private Ecualizador ecualizador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        histograma = new Histograma();
        ecualizador = new Ecualizador();

        imagenOriginal = BitmapFactory.decodeResource(getResources(), R.drawable.salt2);
        imgvResultado = (ImageView) findViewById(R.id.imgvResultado);
    }


    public void procesarImagen(View v)
    {
        try {

            histograma.crearHistograma(imagenOriginal);
            ecualizador = new Ecualizador(imagenOriginal);
            Bitmap imagenEqualizada  = ecualizador.equalizarImagen(histograma.getHistograma());
            imgvResultado.setImageBitmap(imagenEqualizada);
            Log.d("Resultado", "EXITO");
        }
        catch (Exception e) {
            Log.d("Resultado", "Ocurrió un error: " + e.getLocalizedMessage());
        }
    }

    public void procesarImagenConRuido(View v)
    {
        try {
            Bitmap imagenSinRuido = FiltroMediana.filtroMediana(imagenOriginal);
            histograma.crearHistograma(imagenSinRuido);
            ecualizador = new Ecualizador(imagenSinRuido);
            Bitmap imagenEcualizada = ecualizador.equalizarImagen(histograma.getHistograma());
            imgvResultado.setImageBitmap(imagenSinRuido);
            Log.d("Resultado", "EXITO");
        }
        catch (Exception e) {
            Log.d("Resultado", "Ocurrió un error: " + e.getLocalizedMessage());
        }
    }
}
