package com.example.magno_mac.models;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

public class Histograma {
    private int[] histograma = new int[256];

    public Histograma() {
    }

    public boolean crearHistograma(Bitmap imgOri) {

        try {
            histograma= new int[256] ;

            for(int y=0;y<imgOri.getHeight();y++){
                for(int x=0;x<imgOri.getWidth();x++){

                    histograma[getColorGrisPixel(x,y,imgOri)] += 1;

                }
            }

            return true;

        }catch (Exception e)
        {
            Log.d("Error", "Error al Crear Histograma");
            return false;
        }
    }

    public static int getColorGrisPixel(int x,int y, Bitmap imgOri){
        if (x < 0 || x >= imgOri.getWidth() || y < 0 || y >= imgOri.getHeight()) {
            return -1;
        }
        int color = imgOri.getPixel(x,y);

        return (Color.red(color)+Color.green(color)+Color.blue(color))/3;
    }

    public int[] getHistograma() {
        return histograma;
    }
}
