package com.example.magno_mac.models;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Magno on 13/11/2015.
 */
public class FiltroMediana {

    public FiltroMediana() {
    }

    public static Bitmap filtroMediana(Bitmap imagenOriginal) {

        Bitmap imgResul = Bitmap.createBitmap(imagenOriginal.getWidth(), imagenOriginal.getHeight(), Bitmap.Config.RGB_565);

        for (int y = 0; y < imagenOriginal.getHeight(); y++) {
            for (int x = 0; x < imagenOriginal.getWidth(); x++) {
                int mediana = damePixelMediana(x, y, imagenOriginal);
                imgResul.setPixel(x, y, Color.rgb(mediana, mediana, mediana));
            }
        }

        return imgResul;

    }

    private static int damePixelMediana(int x, int y, Bitmap imagenOriginal) {
        List<Integer> arrayList = new ArrayList<Integer>(9);

        arrayList.add(Histograma.getColorGrisPixel(x, y, imagenOriginal));
        arrayList.add(Histograma.getColorGrisPixel(x - 1, y, imagenOriginal));
        arrayList.add(Histograma.getColorGrisPixel(x + 1, y, imagenOriginal));
        arrayList.add(Histograma.getColorGrisPixel(x, y - 1, imagenOriginal));
        arrayList.add(Histograma.getColorGrisPixel(x, y + 1, imagenOriginal));
        arrayList.add(Histograma.getColorGrisPixel(x - 1, y + 1, imagenOriginal));
        arrayList.add(Histograma.getColorGrisPixel(x - 1, y - 1, imagenOriginal));
        arrayList.add(Histograma.getColorGrisPixel(x + 1, y - 1, imagenOriginal));
        arrayList.add(Histograma.getColorGrisPixel(x + 1, y + 1, imagenOriginal));

        return hallarMedia(arrayList);

    }

    private static int hallarMedia(List<Integer> arrayList) {
        //Truco feo para evitar que cambien los indexes mientras se recorre el arreglo.
        for (int i = arrayList.size()-1; i>0; i--) {
            if (arrayList.get(i) == -1) {
                arrayList.remove(i);
            }
        }

        Collections.sort(arrayList);

        int size = arrayList.size();

        if (size == 0) {
            return -1;
        }

        if (size % 2 == 0) {
            int numero2 = arrayList.get(size/2);
            int numero1 = arrayList.get((size/2)-1);

            return Math.round((numero1 + numero2)/2);
        }
        else {
            return (arrayList.get(size/2));
        }
    }
}