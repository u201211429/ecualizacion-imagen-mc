package com.example.magno_mac.models;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

public class Ecualizador {

    private Bitmap imagen;

    public Ecualizador(Bitmap imagen) {
        this.imagen = imagen;
    }

    public Ecualizador(){
    }

    public Bitmap escalaDeGrises() {
        try {
            Bitmap imgResul = Bitmap.createBitmap(imagen.getWidth(), imagen.getHeight(), Bitmap.Config.RGB_565);
            for (int y = 0; y < imagen.getHeight(); y++) {
                for (int x = 0; x < imagen.getWidth(); x++) {

                    imgResul.setPixel(x, y, Histograma.getColorGrisPixel(x, y, imagen));
                }
            }
            return imgResul;

        } catch (Exception e) {
            Log.d("Error", "Error al crear escala de grises: " + e.getLocalizedMessage());
            return null;
        }
    }

    public Bitmap equalizarImagen(int[] histograma) {
        try {
            int[] eq = new int[256];
            int[] reseq = new int[256];

            int suma = 0;
            int gris;

            Bitmap bitmapGris = escalaDeGrises();

            for (int i = 0; i < histograma.length; i++) {
                suma = suma + histograma[i];
                eq[i] = eq[i] + suma;
            }

            for (int i = 0; i < histograma.length; i++) {
                reseq[i] = (int) ((255.0 / (double) suma) * (double) eq[i]);
            }

            for (int y = 0; y < imagen.getHeight(); y++) {
                for (int x = 0; x < imagen.getWidth(); x++) {

                    gris = reseq[Histograma.getColorGrisPixel(x, y, imagen)];
                    bitmapGris.setPixel(x, y, Color.rgb(gris, gris, gris));

                }
            }

            return bitmapGris;

        } catch (Exception e) {
            Log.d("Error", "Error al equalizar imagen:" + e.getLocalizedMessage());
            return null;
        }
    }
}
